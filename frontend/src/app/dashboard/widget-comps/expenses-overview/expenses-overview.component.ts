import { Component } from '@angular/core';

@Component({
  selector: 'app-expenses-overview',
  templateUrl: './expenses-overview.component.html',
  styleUrls: ['./expenses-overview.component.scss']
})
export class ExpensesOverviewComponent {

  expensesData = [
    {
      expenseTitle: 'Licensing',
      expenseTotal: 766980,
      totalBudget: 1000000
    },
    {
      expenseTitle: 'Advertising',
      expenseTotal: 457687,
      totalBudget: 1000000
    },
    {
      expenseTitle: 'Payroll',
      expenseTotal: 997080,
      totalBudget: 1000000
    },
    {
      expenseTitle: 'OpEx',
      expenseTotal: 564900,
      totalBudget: 1000000
    },

  ]

}
