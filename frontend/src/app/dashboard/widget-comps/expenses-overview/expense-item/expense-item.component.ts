import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-expense-item',
  templateUrl: './expense-item.component.html',
  styleUrls: ['./expense-item.component.scss']
})
export class ExpenseItemComponent {

  @Input() expensesData = {
    expenseTitle: '',
    expenseTotal: 0,
    totalBudget: 0
  }

}
