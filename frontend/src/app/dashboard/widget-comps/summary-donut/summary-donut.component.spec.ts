import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SummaryDonutComponent } from './summary-donut.component';

describe('SummaryDonutComponent', () => {
  let component: SummaryDonutComponent;
  let fixture: ComponentFixture<SummaryDonutComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SummaryDonutComponent]
    });
    fixture = TestBed.createComponent(SummaryDonutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
