import { Component, OnInit } from '@angular/core';
import Chart from 'chart.js/auto';

@Component({
  selector: 'app-summary-donut',
  templateUrl: './summary-donut.component.html',
  styleUrls: ['./summary-donut.component.scss']
})
export class SummaryDonutComponent implements OnInit {
  public chart: any;


  ngOnInit(): void {
    this.createChart();

  }
  createChart() {

    this.chart = new Chart("donutChart", {
      type: 'doughnut',

      data: {
        labels: ['Jan', 'Feb', 'Mar'],
        datasets: [
          {
            label: "Sales",
            data: ['467', '576', '572'],
            backgroundColor: ['#FF153C', '#6C8FFF', '#63737A'],
          },
        ]
      },
      options: {
        responsive: false,
        aspectRatio: 1,
        cutout: 50,
        plugins: {
          legend: {
            position: 'bottom',
            labels: {
              boxWidth: 10
            }
          },
          tooltip: {
            enabled: true
          }
        },

      }




    });
  }


}
