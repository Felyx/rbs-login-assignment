import { Component, OnInit } from '@angular/core';
import Chart from 'chart.js/auto'

@Component({
  selector: 'app-stats-overview',
  templateUrl: './stats-overview.component.html',
  styleUrls: ['./stats-overview.component.scss']
})
export class StatsOverviewComponent implements OnInit {
  public chart: any;

  ngOnInit(): void {
    this.createChart();
  }


  createChart() {

    this.chart = new Chart("statsChart", {
      type: 'line',

      data: {
        labels: ['Jan', 'Feb', 'Mar', 'Apr',
          'Jun', 'Jul', 'Aug', 'Sep',],
        datasets: [
          {
            label: "Sales",
            data: ['467', '576', '572', '79', '92',
              '574', '573', '576'],
            borderColor: 'rgb(249, 65, 68)',
            fill: true,
            backgroundColor: 'rgba(249, 65, 68, 0.3)',
            tension: 0.5
          },
          {
            label: "Profit",
            data: ['542', '542', '536', '327', '17',
              '0.00', '538', '541'],
            borderColor: 'rgb(108, 143, 255)',
            fill: true,
            backgroundColor: 'rgba(108, 143, 255, 0.3)',
            tension: 0.5
          }
        ]
      },
      options: {
        scales: {
          x: {
            grid: {
              display: false
            }
          },
          y: {
            grid: {
              display: true
            }
          }
        },

        aspectRatio: 5.5,
        plugins: {
          legend: {
            display: false
          },
        },
      }

    });
  }

}
