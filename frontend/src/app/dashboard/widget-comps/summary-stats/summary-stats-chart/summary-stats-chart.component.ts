import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-summary-stats-chart',
  templateUrl: './summary-stats-chart.component.html',
  styleUrls: ['./summary-stats-chart.component.scss']
})
export class SummaryStatsChartComponent {
  @Input() targetData = {
    caption: '',
    targetValue: 0,
    achievedValue: 0
  }

}
