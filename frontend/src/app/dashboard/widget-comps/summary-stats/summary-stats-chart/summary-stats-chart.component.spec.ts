import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SummaryStatsChartComponent } from './summary-stats-chart.component';

describe('SummaryStatsChartComponent', () => {
  let component: SummaryStatsChartComponent;
  let fixture: ComponentFixture<SummaryStatsChartComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SummaryStatsChartComponent]
    });
    fixture = TestBed.createComponent(SummaryStatsChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
