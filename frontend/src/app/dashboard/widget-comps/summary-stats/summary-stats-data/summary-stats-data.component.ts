import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-summary-stats-data',
  templateUrl: './summary-stats-data.component.html',
  styleUrls: ['./summary-stats-data.component.scss']
})
export class SummaryStatsDataComponent {

  @Input() data = {
    logo: '',
    value: '',
    caption: ''
  }

}
