import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SummaryStatsDataComponent } from './summary-stats-data.component';

describe('SummaryStatsDataComponent', () => {
  let component: SummaryStatsDataComponent;
  let fixture: ComponentFixture<SummaryStatsDataComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SummaryStatsDataComponent]
    });
    fixture = TestBed.createComponent(SummaryStatsDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
