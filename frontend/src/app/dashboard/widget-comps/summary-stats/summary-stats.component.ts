import { Component } from '@angular/core';

@Component({
  selector: 'app-summary-stats',
  templateUrl: './summary-stats.component.html',
  styleUrls: ['./summary-stats.component.scss']
})
export class SummaryStatsComponent {

  statsData = [
    {
      logo: '../../../../assets/images/equity.png',
      value: '$1,074,974',
      caption: 'Share value'
    },
    {
      logo: '../../../../assets/images/cash-receivables.png',
      value: '$908,987',
      caption: 'Total earnings'
    },
    {
      logo: '../../../../assets/images/customers.png',
      value: '1,983',
      caption: 'Total customers'
    },
    {
      logo: '../../../../assets/images/fixed-savings.png',
      value: '$890,675',
      caption: 'Fixed savings'
    }
  ];

  targetData = {
    caption: 'Revenue target',
    targetValue: 43.1,
    achievedValue: 18.8
  }

}
