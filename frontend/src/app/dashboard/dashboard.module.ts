import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashMainComponent } from './layout-comps/dash-main/dash-main.component';
import { DashSideComponent } from './layout-comps/dash-side/dash-side.component';
import { SummaryStatsComponent } from './widget-comps/summary-stats/summary-stats.component';
import { StatsOverviewComponent } from './widget-comps/stats-overview/stats-overview.component';
import { TopContributorsComponent } from './widget-comps/top-contributors/top-contributors.component';
import { SummaryDonutComponent } from './widget-comps/summary-donut/summary-donut.component';
import { ExpensesOverviewComponent } from './widget-comps/expenses-overview/expenses-overview.component';
import { AdvertComponent } from './widget-comps/advert/advert.component';
import { SummaryStatsDataComponent } from './widget-comps/summary-stats/summary-stats-data/summary-stats-data.component';
import { SummaryStatsChartComponent } from './widget-comps/summary-stats/summary-stats-chart/summary-stats-chart.component';
import { ExpenseItemComponent } from './widget-comps/expenses-overview/expense-item/expense-item.component';

@NgModule({
  declarations: [DashboardComponent, DashMainComponent, DashSideComponent, SummaryStatsComponent, StatsOverviewComponent, TopContributorsComponent, SummaryDonutComponent, ExpensesOverviewComponent, AdvertComponent, SummaryStatsDataComponent, SummaryStatsChartComponent, ExpenseItemComponent],
  imports: [CommonModule, DashboardRoutingModule],
})
export class DashboardModule {}
