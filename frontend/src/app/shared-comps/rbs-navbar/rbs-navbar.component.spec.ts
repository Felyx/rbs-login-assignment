import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RbsNavbarComponent } from './rbs-navbar.component';

describe('RbsNavbarComponent', () => {
  let component: RbsNavbarComponent;
  let fixture: ComponentFixture<RbsNavbarComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [RbsNavbarComponent]
    });
    fixture = TestBed.createComponent(RbsNavbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
