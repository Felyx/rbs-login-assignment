import { Component, inject } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from 'src/app/user-auth/guard/auth.service';

@Component({
  selector: 'app-rbs-navbar',
  templateUrl: './rbs-navbar.component.html',
  styleUrls: ['./rbs-navbar.component.scss']
})
export class RbsNavbarComponent {

  displayMenu = false;
  router = inject(Router);
  authService = inject(AuthService);
  loggedIn$ = this.authService.loggedInStatus$;



  toggleMenu(): void {
    this.displayMenu = !this.displayMenu;
  }

  doLogout(): void {
    this.authService.doLogout();
    this.toggleMenu();
  }

}
