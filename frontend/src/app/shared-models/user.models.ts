
export interface AuthDataModel {
    email: string;
    token: string;
    code: string;
    response: string;
    error_message: string;

}