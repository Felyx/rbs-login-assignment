import { Component, OnInit, OnDestroy, inject } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

import { ActivatedRoute, Router } from '@angular/router';

import { AuthService } from '../../guard/auth.service';
import { Subscription, takeLast, takeWhile } from 'rxjs';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit, OnDestroy {
  authService = inject(AuthService)
  registerAccountForm!: FormGroup;
  isComponentActive = true;


  constructor(
    private formBuilder: FormBuilder,
    private router: Router,

  ) { }

  ngOnInit(): void {
    this.registerAccountForm = this.formBuilder.group({
      email: ['', { validators: [Validators.required, Validators.email], updateOn: 'blur' }],
      password1: ['', [Validators.required, Validators.minLength(8)]],
      password2: ['', {
        validators: [
          Validators.required, this.checkPasswords

        ]

      }],
    });
  }

  get email() {
    return this.registerAccountForm.controls['email'];
  }

  get password1() {
    return this.registerAccountForm.controls['password1'];
  }

  get password2() {
    return this.registerAccountForm.controls['password2'];
  }

  checkPasswords(control: FormControl) {
    const password = control.root.get('password1');
    const confirmPassword = control.value;

    if (password && password.value !== confirmPassword) {
      return { passwordsDoNotMatch: true };
    }

    return null;
  }

  setEmailError(err: boolean) {
    const confirmPasswordControl = this.registerAccountForm.get('email');

    if (confirmPasswordControl) {
      if (err) {
        confirmPasswordControl.setErrors({ emailAlreadyInUse: true });
      } else {
        confirmPasswordControl.setErrors(null);
      }
    }

  }

  onRegisterFormSubmit() {
    this.authService.checkAccount(this.email.value).pipe(
      takeWhile(() => this.isComponentActive)
    ).subscribe({
      next: result => {
        if (result.response === 200) {
          const formData = new FormData();
          formData.append('email', this.email.value);
          formData.append('username', this.email.value.replace(/[^\w ]/, ''));
          formData.append('password', this.password1.value);
          formData.append('password2', this.password2.value);

          this.authService.doRegister(formData).subscribe(result => {
            if (result.response === 'success') {
              console.log(result.response);
              this.router.navigate(['auth/login']);
            }
            else {
              // console.log('error')
            }

          });

        }
        else {
          this.setEmailError(true);

        }
      }
    });
  }

  ngOnDestroy(): void {
    this.isComponentActive = false;
  }




}
