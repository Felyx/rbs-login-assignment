import { Component, OnInit, inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

import { ActivatedRoute, Router } from '@angular/router';

import { AuthService } from '../../guard/auth.service';
import { SnackbarService } from 'src/app/shared-services/snackbar.service';
@Component({
  selector: 'app-set-password',
  templateUrl: './set-password.component.html',
  styleUrls: ['./set-password.component.scss']
})
export class SetPasswordComponent implements OnInit {
  setPasswordForm!: FormGroup;
  userEmail = '';
  userPassword = '';
  snackBarService = inject(SnackbarService)


  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private authService: AuthService,
    // private browserStorageService: BrowserStorageService
  ) { }

  ngOnInit(): void {
    if (localStorage.getItem('email') !== null) {
      this.userEmail = localStorage.getItem('email') as string;
    }

    this.setPasswordForm = this.formBuilder.group({
      password1: ['', [Validators.required, Validators.minLength(8)]],
      password2: ['', {
        validators: [
          Validators.required, this.checkPasswords
        ]

      }],
    });
  }

  get password1() {
    return this.setPasswordForm.controls['password1'];
  }

  get password2() {
    return this.setPasswordForm.controls['password2'];
  }

  checkPasswords(control: FormControl) {
    const password = control.root.get('password1');
    const confirmPassword = control.value;

    if (password && password.value !== confirmPassword) {
      return { passwordsDoNotMatch: true };
    }

    return null;
  }





  resetPassword(): void {
    const formData = new FormData();
    formData.append('email', this.userEmail);
    formData.append('password', this.password1.value);
    this.authService.updatePassword(formData).subscribe({
      next: res => {
        if (res.response === 200) {
          this.snackBarService.openSnackBar('We have successfully updated your password')
          this.router.navigate(['/auth/login'], { relativeTo: this.activatedRoute });

        }
        else {
          this.snackBarService.openSnackBar('We could not update your password');

        }
      }
    });

  }

}
