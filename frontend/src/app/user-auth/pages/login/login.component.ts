
import { Component, OnInit, OnDestroy, inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { takeWhile } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';

import { AuthService } from '../../guard/auth.service';
import { SnackbarService } from 'src/app/shared-services/snackbar.service';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {
  authService = inject(AuthService);
  snackbarService = inject(SnackbarService);
  isComponentActive = true;


  logInDetails!: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private activatedRoute: ActivatedRoute
    // private browserStorageService: BrowserStorageService
  ) { }

  ngOnInit(): void {
    this.logInDetails = this.formBuilder.group({
      email: ['', {
        validators: [
          Validators.required
        ],
        updateOn: 'blur'
      }],
      password: [
        '',
        [Validators.required,
        Validators.minLength(6),
        ]
      ],
    });
  }

  get email() {
    return this.logInDetails.controls['email'];
  }

  get password() {
    return this.logInDetails.controls['password'];
  }

  setAuthError(err: boolean) {
    const authEmailControl = this.logInDetails.get('email');
    const authPasswordControl = this.logInDetails.get('email');

    if (authEmailControl && authPasswordControl) {
      if (err) {
        authEmailControl.setErrors({ invalidCredentials: true });
        authPasswordControl.setErrors({ invalidCredentials: true });
      } else {
        authEmailControl.setErrors(null);
        authPasswordControl.setErrors(null);
      }
    }

  }

  onLoginFormSubmit() {
    const formData = new FormData();
    formData.append('username', this.email.value);
    formData.append('password', this.password.value)
    this.authService.doLogin(formData).pipe(
      takeWhile(() => this.isComponentActive)
    ).subscribe({
      next: result => {
        if (result.response === 'success') {
          // localStorage.setItem('token', result.token);
          // this.router.navigate(['./dashboard'])

          this.authService.handleLogin(result.token)
        }
        else {
          this.setAuthError(true);
          this.snackbarService.openSnackBar('The email or password is incorrect!')
        }
      }
    })
  }

  ngOnDestroy(): void {
    this.isComponentActive = false;
  }








}
