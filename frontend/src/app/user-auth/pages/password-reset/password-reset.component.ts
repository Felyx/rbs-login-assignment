import { Component, OnInit, OnDestroy, inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';


import { AuthService } from '../../guard/auth.service';
import { SnackbarService } from 'src/app/shared-services/snackbar.service';

@Component({
  selector: 'app-password-reset',
  templateUrl: './password-reset.component.html',
  styleUrls: ['./password-reset.component.scss']
})
export class PasswordResetComponent implements OnInit, OnDestroy {
  authService = inject(AuthService)
  snackbarService = inject(SnackbarService)
  passwordResetForm!: FormGroup;
  isComponentActive = true;




  constructor(
    private formBuilder: FormBuilder,
    private router: Router

  ) { }

  ngOnInit(): void {
    this.passwordResetForm = this.formBuilder.group({
      email: ['', {
        validators: [
          Validators.required,
          Validators.email
        ]
      }]
    });
  }

  get email() {
    return this.passwordResetForm.controls['email'];
  }

  setEmailError(err: boolean) {
    const emailControl = this.passwordResetForm.get('email');
    if (emailControl) {
      if (err) {
        emailControl.setErrors({ emailNoExist: true });
      } else {
        emailControl.setErrors(null);
      }
    }

  }


  sendVerificationCode() {
    this.authService.resetPassword(this.email.value).subscribe({
      next: result => {
        if (result.response === this.email.value) {
          console.log('Matched ' + result.response);
          // this.resetCode = result.reset_code;
          // this.resetToken = result.token
          localStorage.setItem('code', result.reset_code)
          localStorage.setItem('token', result.token)
          localStorage.setItem('email', this.email.value)
          this.snackbarService.openSnackBar(`Code generated! However, we are not sending emails. Please use ${result.reset_code} for demo`)
          this.router.navigate(['auth/verify-code'])
        }
        else if (result.response === 'Account does not exist') {
          this.setEmailError(true)
        }
        else {
          console.log(result.response);
        }
      }
    });

  }

  ngOnDestroy(): void {
    this.isComponentActive = false;
  }


}
