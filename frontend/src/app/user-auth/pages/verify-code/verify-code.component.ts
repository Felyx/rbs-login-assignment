import { Component, OnInit, inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';


import { AuthService } from '../../guard/auth.service';
import { SnackbarService } from 'src/app/shared-services/snackbar.service';


@Component({
  selector: 'app-verify-code',
  templateUrl: './verify-code.component.html',
  styleUrls: ['./verify-code.component.scss']
})
export class VerifyCodeComponent {

  verifyCodeForm!: FormGroup;
  snackbarService = inject(SnackbarService)

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    // private browserStorageService: BrowserStorageService
  ) { }

  ngOnInit(): void {
    this.verifyCodeForm = this.formBuilder.group({
      code: [
        '',
        [Validators.required,
        Validators.pattern("^[0-9]*$"),
        Validators.minLength(4),
        Validators.maxLength(4)

        ]
      ],
    });
  }

  get code() {
    return this.verifyCodeForm.controls['code'];
  }


  verifyCode(): void {
    const rCode = localStorage.getItem('code')

    if (rCode === this.code.value) {
      this.router.navigate(['/auth/set-password'])
    } else {
      this.snackbarService.openSnackBar('The provided code is invalid!')

    }
  }






}
