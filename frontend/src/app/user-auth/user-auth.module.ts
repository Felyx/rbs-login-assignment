import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { UserAuthComponent } from './user-auth.component';
import { LoginComponent } from './pages/login/login.component';
import { PasswordResetComponent } from './pages/password-reset/password-reset.component';
import { RegisterComponent } from './pages/register/register.component';
import { SetPasswordComponent } from './pages/set-password/set-password.component';
import { UserAuthRoutingModule } from './user-auth-routing.module';
import { VerifyCodeComponent } from './pages/verify-code/verify-code.component';


@NgModule({
  declarations: [
    UserAuthComponent,
    LoginComponent,
    PasswordResetComponent,
    RegisterComponent,
    SetPasswordComponent,
    VerifyCodeComponent,
  ],
  imports: [CommonModule, FormsModule, ReactiveFormsModule, MatFormFieldModule, MatInputModule, MatSnackBarModule, UserAuthRoutingModule],
})
export class UserAuthModule { }
