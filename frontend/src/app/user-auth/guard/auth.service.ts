import { inject } from '@angular/core';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { Router } from '@angular/router';

import { backendURL } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  http = inject(HttpClient);
  router = inject(Router);
  loggedInStatus$ = new BehaviorSubject<boolean>(this.tokenAvailable());


  doLogin(formData: any): Observable<any> {
    return this.http.post(backendURL + '/users/login', formData,);
  }

  doRegister(formData: any): Observable<any> {
    return this.http.post<any>(backendURL + '/users/register', formData);
  }

  checkAccount(email: string): Observable<any> {
    return this.http.get<any>(backendURL + '/users/check-if-account-exists/?email=' + email);
  }


  resetPassword(email: string): Observable<any> {
    return this.http.get<any>(backendURL + '/users/password-reset/?email=' + email);
  }


  updatePassword(formData: FormData): Observable<any> {
    return this.http.post<any>(backendURL + '/users/change-password/', formData);
  }

  get isUserLoggedIn() {
    return this.loggedInStatus$.asObservable();
  }

  private tokenAvailable(): boolean {
    return !!localStorage.getItem('token');
  }



  handleLogin(payload: string) {
    localStorage.setItem('token', payload);
    this.loggedInStatus$.next(true);
    this.router.navigate(['./dashboard'])
  }

  doLogout(): void {
    localStorage.removeItem('token')
    this.loggedInStatus$.next(false);
    this.router.navigate(['./auth'])

  }
}
