import { NgModule } from '@angular/core';
import { LoginComponent } from './pages/login/login.component';
import { PasswordResetComponent } from './pages/password-reset/password-reset.component';
import { RegisterComponent } from './pages/register/register.component';
import { SetPasswordComponent } from './pages/set-password/set-password.component';
import { RouterModule, Routes } from '@angular/router';
import { UserAuthComponent } from './user-auth.component';
import { VerifyCodeComponent } from './pages/verify-code/verify-code.component';

const userAuthRoutes: Routes = [

  {
    path: '',
    component: UserAuthComponent,
    children: [
      {
        path: '',
        redirectTo: 'login',
        pathMatch: 'full',
      },
      {
        path: 'login',
        component: LoginComponent,
        pathMatch: 'full',
      },
      {
        path: 'register',
        component: RegisterComponent,
        pathMatch: 'full',
      },
      {
        path: 'password-reset',
        component: PasswordResetComponent,
        pathMatch: 'full',
      },
      {
        path: 'set-password',
        component: SetPasswordComponent,
        pathMatch: 'full',
      },
      {
        path: 'verify-code',
        component: VerifyCodeComponent,
        pathMatch: 'full',
      },
    ]
  }

];

@NgModule({
  imports: [RouterModule.forChild(userAuthRoutes)],
  exports: [RouterModule],
})
export class UserAuthRoutingModule { }
