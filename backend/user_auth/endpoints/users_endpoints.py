from django.urls import path
from ..api.users_api import(
	registration_view,
	ObtainAuthTokenView,
	account_properties_view,
	update_account_view,
	password_reset_view,
	check_if_account_exists,
	password_reset,
	ChangePasswordView,
)
from rest_framework.authtoken.views import obtain_auth_token

app_name = 'account'

urlpatterns = [
	path('check-if-account-exists/', check_if_account_exists, name="check_if_account_exists"),
	path('password-reset/', password_reset_view, name="perform_password_reset"),
	path('change-password/', password_reset, name="change_password"),
	path('properties', account_properties_view, name="properties"),
	path('properties/update', update_account_view, name="update"),
	path('login', ObtainAuthTokenView.as_view(), name="login"), 
	path('register', registration_view, name="register"),
]