from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework import generics
from rest_framework.generics import UpdateAPIView, ListAPIView
from django.contrib.auth import authenticate
from rest_framework.authentication import TokenAuthentication
from rest_framework.decorators import api_view, authentication_classes, permission_classes

from ..serializers.users_serializers import RegistrationSerializer, AccountPropertiesSerializer, ChangePasswordSerializer
from ..models import Account
from rest_framework.authtoken.models import Token

from django.core.mail import send_mail
from django.template.loader import render_to_string
from django.core.mail import EmailMessage

from django.dispatch import receiver
from django.urls import reverse
from django_rest_passwordreset.signals import reset_password_token_created


import random

# Handle user registration
@api_view(['POST', ])
@permission_classes([])
@authentication_classes([])
def registration_view(request):

	if request.method == 'POST':
		data = {}
		email = request.data.get('email', '0').lower()
		if validate_email(email) != None:
			data['error_message'] = 'That email is already in use.'
			data['response'] = 'error'
			return Response(data)

		

		username = request.data.get('username', '0')
		if validate_username(username) != None:
			data['error_message'] = 'That username is already in use.'
			data['response'] = 'error'
			return Response(data)

		serializer = RegistrationSerializer(data=request.data)
		
		if serializer.is_valid():
			account = serializer.save()
			data['response'] = 'success'
			data['email'] = account.email
			data['username'] = account.username
			data['pk'] = account.pk
			token = Token.objects.get(user=account).key
			data['token'] = token
		else:
			data = serializer.errors
			print(data)
		return Response(data)

def validate_email(email):
	account = None
	try:
		account = Account.objects.get(email=email)
	except Account.DoesNotExist:
		return None
	if account != None:
		return email

def validate_username(username):
	account = None
	try:
		account = Account.objects.get(username=username)
	except Account.DoesNotExist:
		return None
	if account != None:
		return username



@api_view(['GET', ])
@permission_classes((IsAuthenticated, ))
def account_properties_view(request):

	try:
		account = request.user
	except Account.DoesNotExist:
		return Response(status=status.HTTP_404_NOT_FOUND)

	if request.method == 'GET':
		serializer = AccountPropertiesSerializer(account)
		return Response(serializer.data)


# Handle update account
@api_view(['PUT',])
@permission_classes((IsAuthenticated, ))
def update_account_view(request):

	try:
		account = request.user
	except Account.DoesNotExist:
		return Response(status=status.HTTP_404_NOT_FOUND)
		
	if request.method == 'PUT':
		serializer = AccountPropertiesSerializer(account, data=request.data)
		data = {}
		if serializer.is_valid():
			serializer.save()
			data['response'] = 'Account update success'
			return Response(data=data)
		return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)



# Handle user login
class ObtainAuthTokenView(APIView):

	authentication_classes = []
	permission_classes = []

	def post(self, request):
		context = {}

		email = request.POST.get('username')
		password = request.POST.get('password')
		account = authenticate(email=email, password=password)
		if account:
			try:
				token = Token.objects.get(user=account)
			except Token.DoesNotExist:
				token = Token.objects.create(user=account)
			context['response'] = 'success'
			context['pk'] = account.pk
			context['email'] = email.lower()
			context['token'] = token.key
		else:
			context['response'] = 'error'
			context['error_message'] = 'Invalid credentials'

		return Response(context)


@api_view(['GET', ])
@permission_classes([])
@authentication_classes([])
def check_if_account_exists(request):
	if request.method == 'GET':
		email = request.GET['email'].lower()
		data = {}
		try:
			account = Account.objects.get(email=email)
			data['response'] = 500
			data['message'] = "Email already exists"
		except Account.DoesNotExist:
			data['response'] = 200
			data['message'] = "Account does not exist"
			print(data)
		return Response(data)


@api_view(['POST', ])
@permission_classes([])
@authentication_classes([])
def password_reset(request):
	if request.method == 'POST':
		email = request.data.get('email', '0').lower()
		password = request.data.get('password', '0')
		data = {}
		try:
			account = Account.objects.get(email=email)
			print(account.password)
			account.set_password(password)
			account.save()
			print('---------------')
			print(account.password)
			data['response'] = 200
			data['message'] = "Password was changed successfully"
		except Account.DoesNotExist:
			data['response'] = "Account does not exist"
		return Response(data)


@api_view(['GET', ])
@permission_classes([])
@authentication_classes([])
def password_reset_view(request):
	if request.method == 'GET':
		email = request.GET['email'].lower()
		reset_code = random.randint(1000,9999)
		data = {}
		try:
			account = Account.objects.get(email=email)
			token = Token.objects.get(user=account)
			print(token)
			username = account.username
			print(username)
			data['response'] = email
			data['reset_code'] = reset_code
			data['token'] = str(token)
			email_body = 'Email body'
			email = EmailMessage('Email subject', email_body, to=[email])
			email.content_subtype = "html" 
			# email.send() #Not sending email for this implementation

		except Account.DoesNotExist:
			data['response'] = "Account does not exist"
		return Response(data)



# Handle change password
class ChangePasswordView(UpdateAPIView):

	serializer_class = ChangePasswordSerializer
	model = Account
	permission_classes = (IsAuthenticated,)
	authentication_classes = (TokenAuthentication,)


	def get_object(self, queryset=None):
		obj = self.request.user
		return obj

	def update(self, request, *args, **kwargs):
		self.object = self.get_object()
		serializer = self.get_serializer(data=request.data)

		if serializer.is_valid():
			# Check old password
			if not self.object.check_password(serializer.data.get("old_password")):
				return Response({"old_password": ["Wrong password."]}, status=status.HTTP_400_BAD_REQUEST)

			# confirm the new passwords match
			new_password = serializer.data.get("new_password")
			confirm_new_password = serializer.data.get("confirm_new_password")
			if new_password != confirm_new_password:
				return Response({"new_password": ["New passwords must match"]}, status=status.HTTP_400_BAD_REQUEST)

			# set_password also hashes the password that the user will get
			self.object.set_password(serializer.data.get("new_password"))
			self.object.save()
			return Response({"response":"successfully changed password"}, status=status.HTTP_200_OK)

		return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)



@receiver(reset_password_token_created)
def password_reset_token_created(sender, instance, reset_password_token, *args, **kwargs):
	email_plaintext_message = "{}?token={}".format(reverse('password_reset:reset-password-request'), reset_password_token.key)
	print(reset_password_token.key)

	email_body = 'Email body'
	email = EmailMessage('Email subject', email_body, to=[reset_password_token.user.email])
	email.content_subtype = "html" 
	email.send()
	


