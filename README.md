# rbs-login-assignment

## Running the backend on dev machine

With terminal inside the backend folder, run

1. pip install -r requirements.txt
2. python3 manage.py runserver 127.0.0.1:8000

## Running the frontend on dev machine

With terminal inside the frontend folder, and node version 18^already installed, run:

1. npm install
2. npm run start

The project will be accessible through a browser and by visiting http://localhost:4200
